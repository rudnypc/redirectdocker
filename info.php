<?php
function log_time($message) {
    $time = date('Y-m-d H:i:s');
    $log_message = "[$time] $message\n";
    file_put_contents('debug_timing.log', $log_message, FILE_APPEND);
}

function base64_url_encode($input) {
    return strtr(base64_encode($input), '+/=', '._-');
}

function base64_url_decode($input) {
    return base64_decode(strtr($input, '._-', '+/='));
}

function encode_basename($url) {
    $url = explode('/', $url);
    $base = array_pop($url);
    return implode('/', $url) . '/' . rawurlencode($base);
}

function verifica($conta) {
    log_time("Iniciando verifica para conta: $conta");
    $online = false;
    $valor = trim(shell_exec("sudo /usr/bin/docker ps --format '{{.Names}}'"));
    $array = preg_split("/\r\n|\n|\r/", $valor);

    foreach ($array as $value) {
        if ($conta == $value) {
            $port = trim(shell_exec("sudo docker port " . $value . " 2>&1 | head -n 1 | sed 's/^.*://'"));
            $online = $port;
            break;
        }
    }
    log_time("Iniciando troca nginx");

 /*   if ($online) {
        $config_file = "/etc/nginx/configs/" . $online . ".conf";
        $fp = fopen($config_file, 'w');
        fwrite($fp, "location /$online/ { proxy_pass http://127.0.0.1:$online/; }");
        fclose($fp);
        exec("/usr/sbin/service nginx reload");
        log_time("Configuração Nginx atualizada para porta: $online");
    } */

    return $online;
}

$start_time = microtime(true);
$retorno = verifica("files_" . $_GET['id']);
log_time("Verifica retornou: " . ($retorno ? $retorno : "false"));

if (!$retorno) {
    log_time("Container não encontrado, iniciando criação...");
    shell_exec("sudo /usr/bin/docker rm files_" . $_GET['id'] . "");
    mkdir("/mnt/s3/data/" . $_GET['id'], 0700);
    shell_exec("sudo /usr/bin/docker run -d --privileged -e PORT=4040 -e id=" . $_GET['id'] . " -e auth=" . $_GET['api'] . " --mount type=bind,source=/mnt/mounts,target=/mnt/,bind-propagation=shared --device /dev/fuse --cap-add SYS_ADMIN -p 0:4040 --dns 2001:4860:4860::8888 --name files_" . $_GET['id'] . " rudnypc/teste:2.0");
    log_time("Container criado.");
    shell_exec("sudo /usr/bin/docker start files_" . $_GET['id'] . "");
    $retorno = verifica("files_" . $_GET['id']);
}

if ($retorno) {
    $url = 'http://127.0.0.1:' . $retorno . '/' . base64_url_decode($_GET['url']);
    $i = 1;
    while ($i <= 20) {
        if (file_exists("files/" . base64_decode($_GET['url']))) {
            log_time("Arquivo encontrado localmente.");
            break;
        }
        if (checkcode($url)) {
            log_time("Código 200 recebido da URL: $url");
            break;
        }
        sleep(1);
        $i++;
    }
    if ($_GET['hls']) {
        // Processamento HLS omitido para brevidade.
    } elseif (!$_GET['redir']) {
        header('Location: ' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/files/' . encode_basename(base64_url_decode($_GET['url'])));
    } else {
        header('Location: http://' . $_GET['redir'] . '/' . base64_decode($_GET['url']) . '?address=' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . ':' . $retorno);
    }
}

$end_time = microtime(true);
log_time("Script concluído em " . ($end_time - $start_time) . " segundos.");

function checkcode($url) {
    $header = (get_headers($url, 1)[0]);
    return strpos($header, "200") !== false;
}
